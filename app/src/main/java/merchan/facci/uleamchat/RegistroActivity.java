package merchan.facci.uleamchat;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import merchan.facci.uleamchat.Modelo.User;


public class RegistroActivity extends AppCompatActivity {
    EditText nombreEt, emailEt, passwordEt, FacultadEt, telefonoEt,sexoEt;
    Button btn_register;
    FirebaseAuth auth;
    DatabaseReference reference;
    TextView mHaveAcoount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Registro");
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);

        nombreEt = findViewById(R.id.nombreEt);
        mHaveAcoount = findViewById(R.id.have_accountTv);
        emailEt = findViewById(R.id.emailEt);
        FacultadEt = findViewById(R.id.FacultadEt);
        telefonoEt = findViewById(R.id.telefonoEt);
        sexoEt = findViewById(R.id.sexoEt);
        passwordEt = findViewById(R.id.passwordEt);
        btn_register = findViewById(R.id.btn_register);
        auth = FirebaseAuth.getInstance();

        mHaveAcoount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistroActivity.this, MainActivity.class));
                finish();
            }
        });


        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String txt_usuario = nombreEt.getText().toString();
                String txt_correo = emailEt.getText().toString();
                String txt_contraseña = passwordEt.getText().toString();
                String txt_facultad = FacultadEt.getText().toString();
                String txt_telefono = telefonoEt.getText().toString();
                String txt_sexo = sexoEt.getText().toString();

                if (TextUtils.isEmpty(txt_usuario) || TextUtils.isEmpty(txt_correo)|| TextUtils.isEmpty(txt_facultad)|| TextUtils.isEmpty(txt_telefono)|| TextUtils.isEmpty(txt_sexo) || TextUtils.isEmpty(txt_contraseña)) {
                    Toast.makeText(RegistroActivity.this, "Ningun campo debe estar vacio", Toast.LENGTH_SHORT).show();
                }else if (txt_contraseña.length()<6){
                    Toast.makeText(RegistroActivity.this,"ERROR: La contraseña debe tener almenos 6 caracteres",Toast.LENGTH_SHORT).show();
                }else {

                    register(txt_usuario,txt_correo, txt_contraseña, txt_facultad, txt_telefono,txt_sexo);
                }

            }
        });
    }

    private void register(final String username, final String email, String password, final String facultad, final String telefono, final String sexo){
        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            assert firebaseUser != null;
                            String userid = firebaseUser.getUid();
                            reference = FirebaseDatabase.getInstance().getReference("Users").child(userid);
                            User user = new User();
                            user.setUid(userid);
                            user.setName(username);
                            user.setImage("");
                            user.setEmail(email);
                            user.setFaculty(facultad);
                            user.setPhone(telefono);
                            user.setGender(sexo);

                            reference.setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        Intent intent = new Intent(RegistroActivity.this, MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            });
                        }else {
                            Toast.makeText(RegistroActivity.this,"tienes problemas con el correo o contrasena",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}